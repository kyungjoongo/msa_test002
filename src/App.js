import './App.css';
import 'antd-mobile/dist/antd-mobile.css';
import React from "react";
import FlatListScreen from "./FlatListScreen";

function App() {
    return (
        <FlatListScreen/>
    );
}
export default App;
