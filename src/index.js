import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import reportWebVitals from './reportWebVitals';
import FlatListScreen from "./FlatListScreen";
import 'antd-mobile/lib/date-picker/style/css';
ReactDOM.render(
    <React.StrictMode>
        <FlatListScreen/>
    </React.StrictMode>,
    document.getElementById('root')
);

reportWebVitals();
