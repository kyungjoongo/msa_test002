

type TypeCardListOne = {
    aliases: Array,
    allegiances: Array,
    books: Array,
    born: string,
    culture: string,
    died: string,
    father: string,
    gender: string,
    mother: string,
    name: string,
    playedBy: Array,
    povBooks: Array,
    spouse:  string,
    titles: Array,
    tvSeries:  string,
    url:  string,
}