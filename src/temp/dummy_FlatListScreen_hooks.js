// @flow
import React, {useEffect, useState} from 'react';
import {ActivityIndicator, SafeAreaView, StyleSheet, Text, View} from 'react-native-web';
import axios from 'axios';
import _ from 'lodash';
import FlatList from 'flatlist-react'

export default function Dummy_FlatListScreen_hooks() {

    const [loading, setLoading] = useState(false);
    const [page, setPage] = useState(1);
    const [results, setResults] = useState([]);
    const [moreItems, setMoreItems] = useState(true);

    useEffect(() => {
        (async function () {
            try {
                setLoading(true);
                let results = await getList();
                setPage(page + 1);
                setResults(results)
                setLoading(false);

            } catch (e) {
                console.error(e);
            }
        })();
    }, []);


    const ItemView = (item, index) => {
        return (
            // FlatList Item
            <View key={index}>
                <Text
                    style={styles.item}
                    onPress={() => {
                    }}>

                    {_.isEmpty(item.name) ? "---" : item.name}
                </Text>
            </View>
        );
    };

    async function getList() {
        console.log("page====>", page);
        return await axios({
            url: `https://www.anapioficeandfire.com/api/characters?page=${page}&pageSize=20`,
            method: 'get',

        }).then(res => {
            console.log("data====>", res.data);
            return res.data;
        })

    }

    async function getMoreListAndAppendToList() {
        setLoading(true);
        let prevList = results;
        let moreList = await getList();
        let mergeList = prevList.concat(moreList);
        setResults(mergeList)
        setLoading(false);
        setPage(page + 1);
    }

    return (
        <FlatList
            list={results}
            renderItem={ItemView}
            renderWhenEmpty={() => <div>List is empty!</div>}
            hasMoreItems={moreItems}
            loadMoreItems={async () => {
                if (!loading) {
                    await getMoreListAndAppendToList();
                }
            }}
        />
    )

}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        marginTop: 30,
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },
});
