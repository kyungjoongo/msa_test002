// @flow
import React, {useEffect, useState} from 'react';

import {ActivityIndicator, SafeAreaView, StyleSheet, Text, View} from 'react-native-web';
import axios from 'axios';
import _ from 'lodash';
import FlatList from 'flatlist-react'
import {Button} from "antd-mobile";
import Tag from "antd-mobile/es/tag";
import 'antd-mobile/dist/antd-mobile.css';


type Props = {};
type State = {
    loading: boolean,
    page: number,
    results: Array,
    moreItems: boolean,
    gender: string,
    isNotExistTvSeries: boolean,
    died: string,
    topLoading: boolean,
};


export default class FlatListScreen extends React.Component<Props, State> {

    constructor() {
        super();
        this.state = {
            loading: false,
            page: 1,
            results: [],
            moreItems: true,
            gender: undefined,
            isNotExistTvSeries: undefined,
            died: undefined,
            topLoading: false,
        }
    }

    async componentDidMount() {
        let results = await this.getList();

        this.setState({
            results: results,
        })
    }

    ItemView = (item: TypeCardListOne, index) => {
        return (
            // FlatList Item
            <View key={index} style={{background: 'grey', margin: 10, height: 150}}>

                <View style={{flexDirection: 'row', flex: 1, height: 50}}>
                    <View style={{flex: .3, margin: 10}}>
                        <Text
                            style={{fontSize: 15,}}
                            onPress={() => {
                            }}>

                            name:{_.isEmpty(item.name) ? "무제" : item.name}
                        </Text>
                    </View>
                    <View style={{flex: .7, margin: 10}}>
                        <Text
                            style={{fontSize: 15,}}
                            onPress={() => {
                            }}>

                            aliases:{item.aliases.toString()}
                        </Text>
                    </View>
                </View>
                <View style={{flexDirection: 'row', flex: 1, margin: 10,}}>
                    <View style={{flex: .7}}>
                        <Text>
                            title: {item.titles.toString()} -
                        </Text>
                    </View>
                    <View style={{flex: .3}}>
                        <button type="primary" onClick={() => {
                            alert('sdflksdlfk')
                        }}>삭제
                        </button>
                    </View>
                </View>
                {/*todo: gender*/}
                {/*todo: gender*/}
                {/*todo: gender*/}
                <View style={{margin: 10}}>
                    <Text style={{color: 'red'}}>
                        {item.gender}
                    </Text>
                </View>
                {/*todo: died*/}
                {/*todo: died*/}
                {/*todo: died*/}
                <View style={{margin: 10}}>
                    <Text style={{color: 'red'}}>
                        {item.died}
                    </Text>
                </View>
                <View style={{flexDirection: 'row', flex: 1, margin: 10, height: 30}}>
                    <View style={{flex: .5}}>
                        <Text>
                            books: {item.books.length}
                        </Text>
                    </View>
                    <View style={{flex: .5}}>
                        <Text>
                            tvSeries: {_.isEmpty(item.tvSeries.toString()) ? 0 : item.tvSeries.length}
                        </Text>
                    </View>
                </View>
            </View>
        );
    };


    async getList() {
        console.log("page====>", this.state.page);

        let mergeArrList = []
        try {
            return await axios({
                url: `https://www.anapioficeandfire.com/api/characters?page=${this.state.page}&pageSize=20&gender=${this.state.gender}`,
                method: 'get',

            }).then(res => {
                console.log("data====>", res.data);

                let filterArrayList = []
                //todo: 생존인물만 필터링
                if (this.state.died === '') {
                    let arrList = []
                    res.data.map(item => {
                        if (item.died === '') {
                            arrList.push(item)
                        }
                    })

                    filterArrayList = arrList;
                } else {
                    filterArrayList = res.data;
                }

                let filterArrayList2 = []
                if (this.state.isNotExistTvSeries) {
                    filterArrayList.map(item => {

                        console.log("tvSeries====>",);

                        if (_.isEmpty(item.tvSeries.toString())) {
                            filterArrayList2.push(item)
                        }
                    })

                    mergeArrList = filterArrayList2;
                } else {
                    mergeArrList = filterArrayList;
                }

                return mergeArrList

            })
        } catch (e) {
            alert(e.toString())
        }


    }

    async getMoreListAndAppendToList() {
        let prevList = this.state.results;
        let moreList = await this.getList();
        let mergeList = prevList.concat(moreList);

        if (moreList.length === 0) {
            await this.setState({
                moreItems: false,
            })
        }

        setTimeout(() => {
            this.setState({
                results: mergeList,
                loading: false,
                page: this.state.page + 1,
            })
        }, 2000)

    }

    renderFilter() {
        return (
            <View style={{margin: 10, flexDirection: 'row', justifyContent: 'space-between'}}>
                <Tag selected={this.state.died !== undefined ? true : false} onChange={async (value) => {
                    if (value) {
                        await this.setState({
                            died: '',
                            results: [],
                            page: 1,
                        })
                    } else {
                        await this.setState({
                            died: 'dummy_klfsd',
                            results: [],
                            page: 1,
                        })
                    }

                    await this.setState({topLoading: true,})
                    let results = await this.getList();

                    this.setState({
                        results: results,
                        topLoading: false,
                    })
                }}>
                    생존인물만
                </Tag>
                <Tag selected={this.state.gender !== undefined ? true : false} onChange={async (value) => {

                    if (value) {
                        await this.setState({
                            gender: 'Female',
                            results: [],
                            page: 1,
                        })
                    } else {
                        await this.setState({
                            gender: 'Male',
                            results: [],
                            page: 1,
                        })
                    }

                    await this.setState({topLoading: true,})
                    let results = await this.getList();

                    this.setState({
                        results: results,
                        topLoading: false,
                    })
                }}>
                    여자
                </Tag>
                <Tag selected={this.state.isNotExistTvSeries !== undefined ? true : false} onChange={async (value) => {

                    if (value) {
                        await this.setState({
                            isNotExistTvSeries: true,
                            results: [],
                            page: 1,
                        })
                    } else {
                        await this.setState({
                            isNotExistTvSeries: false,
                            results: [],
                            page: 1,
                        })
                    }

                    await this.setState({topLoading: true,})
                    let results = await this.getList();

                    this.setState({
                        results: results,
                        topLoading: false,
                    })
                }}>
                    tv씨리즈 없음
                </Tag>

                {/*todo: 초기화*/}
                {/*todo: 초기화*/}
                {/*todo: 초기화*/}
                {/*todo: 초기화*/}
                <Tag onChange={async (value) => {
                    await this.setState({
                        isNotExistTvSeries: undefined,
                        gender: '',
                        died: '',
                        results: [],
                        page: 1,
                    })
                    await this.setState({topLoading: true,})
                    let results = await this.getList();

                    this.setState({
                        results: results,
                        topLoading: false,
                    })
                }}>
                    초기화
                </Tag>
            </View>
        )
    }

    render() {
        return (
            <div>
                <View style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    height: 80,
                    margin: 10,
                    background: 'green'
                }}>
                    <Text style={{fontSize: 25}}> 무신사 과제</Text>
                </View>

                {this.renderFilter()}
                {this.state.topLoading && <ActivityIndicator color={'green'} size={'large'}/>}
                <FlatList
                    list={this.state.results}
                    renderItem={this.ItemView}
                    hasMoreItems={this.state.moreItems}
                    loadMoreItems={async () => {
                        if (!this.state.loading) {
                            await this.getMoreListAndAppendToList();
                        }
                    }}
                    paginationLoadingIndicator={() => {
                        return (
                            <div style={{
                                bottom: 50,
                            }}>
                                <ActivityIndicator color={'red'} size={'large'}/>
                            </div>
                        )
                    }}
                    paginationLoadingIndicatorPosition={'center'}
                    renderWhenEmpty={() => {
                        if (this.state.loading) {
                            return (
                                <div style={{marginBottom: 150, zIndex: 99999, position: 'absolute'}}>

                                </div>
                            )
                        } else {
                            return (
                                <div>

                                </div>
                            )
                        }
                    }}
                />
            </div>
        );
    };
};


const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        flex: 1,
        marginLeft: 10,
        marginRight: 10,
        marginBottom: 10,
        marginTop: 30,
    },
    item: {
        padding: 10,
        fontSize: 18,
        height: 44,
    },
});
